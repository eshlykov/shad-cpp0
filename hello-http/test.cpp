#include <catch.hpp>

#include <Poco/URI.h>
#include <Poco/Net/ServerSocket.h>
#include <Poco/Net/HTTPServer.h>
#include <Poco/Net/HTTPServerRequest.h>
#include <Poco/Net/HTTPServerResponse.h>
#include <Poco/Net/HTTPRequestHandler.h>
#include <Poco/Net/HTTPRequestHandlerFactory.h>

#include <stdexcept>
#include <algorithm>
#include <cmath>
#include <optional>
#include <iostream>

#include <fake_data.h>
#include <weather.h>

static const std::string kTestApiKey = "abcdefgh";

class FakeHandler : public Poco::Net::HTTPRequestHandler {
public:
    virtual void handleRequest(Poco::Net::HTTPServerRequest& /*request*/,
                               Poco::Net::HTTPServerResponse& response) override {
        response.setStatus(Poco::Net::HTTPResponse::HTTP_BAD_REQUEST);
        response.send();
    }
};

bool ExpectMethod(Poco::Net::HTTPRequest& request, const std::string& method) {
    if (request.getMethod() != method) {
        std::cerr << "Invalid method: expected " + method + ", got " + request.getMethod() << "\n";
        return true;
    }
    return false;
}

bool ExpectKey(Poco::Net::HTTPRequest& request, const std::string& key) {
    if (request.get("X-Yandex-API-Key", "") != key) {
        std::cerr << "Invalid api key: expected " + key << "\n";
        return true;
    }
    return false;
}

class DefaultHandler : public FakeHandler {
public:
    virtual void handleRequest(Poco::Net::HTTPServerRequest& request,
                               Poco::Net::HTTPServerResponse& response) override {
        bool fail = false;
        Poco::URI uri(request.getURI());
        fail |= uri.getPath() != "/v1/forecast";
        fail |= ExpectMethod(request, "GET");
        fail |= ExpectKey(request, kTestApiKey);

        if (fail) {
            FakeHandler::handleRequest(request, response);
            return;
        }

        response.setStatus(Poco::Net::HTTPResponse::HTTP_OK);
        response.send() << FakeData::moscow_weather;
    }
};

class LatLonHandler : public FakeHandler {
public:
    virtual void handleRequest(Poco::Net::HTTPServerRequest& request,
                               Poco::Net::HTTPServerResponse& response) override {
        bool fail = false;
        fail |= ExpectMethod(request, "GET");
        fail |= ExpectKey(request, kTestApiKey);
        Poco::URI uri(request.getURI());
        auto params = uri.getQueryParameters();
        std::sort(params.begin(), params.end());
        fail |= (params.size() != 2 || params[0].first != "lat" || params[1].first != "lon");

        if (!fail) {
            fail |= (CheckValue(params[0].second, 59.6) || CheckValue(params[1].second, 30.2));
        }

        if (fail) {
            FakeHandler::handleRequest(request, response);
            return;
        }

        response.setStatus(Poco::Net::HTTPResponse::HTTP_OK);
        response.send() << FakeData::spb_weather;
    }

private:
    bool CheckValue(const std::string& actual, double expected) {
        double value;
        try {
            value = std::stod(actual);
        } catch (const std::exception& e) {
            std::cerr << e.what() << "\n";
            return true;
        }
        if (std::fabs(value - expected) > 0.1) {
            return true;
        }
        return false;
    }
};

class FakeHandlerFactory : public Poco::Net::HTTPRequestHandlerFactory {
public:
    FakeHandlerFactory(std::string test) : test_(std::move(test)) {
    }

    virtual Poco::Net::HTTPRequestHandler* createRequestHandler(
        const Poco::Net::HTTPServerRequest&) {
        if (test_ == "error") {
            return new FakeHandler();
        } else if (test_ == "moscow weather") {
            return new DefaultHandler();
        } else if (test_ == "spb weather") {
            return new LatLonHandler();
        } else {
            throw std::runtime_error("Unknown test type");
        }
    }

private:
    std::string test_;
};

class FakeServer {
public:
    FakeServer(std::string test) : test_(std::move(test)) {
    }

    ~FakeServer() {
        Stop();
    }

    void Start() {
        socket_.reset(new Poco::Net::ServerSocket(Poco::Net::SocketAddress("localhost", 0)));

        server_.reset(new Poco::Net::HTTPServer(new FakeHandlerFactory(test_), *socket_,
                                                new Poco::Net::HTTPServerParams()));

        server_->start();
    }

    std::string GetUrl() {
        Poco::URI uri;
        uri.setScheme("http");
        uri.setHost("localhost");
        uri.setPort(socket_->address().port());
        uri.setPath("/v1/forecast");
        return uri.toString();
    }

    void Stop() {
        if (server_) {
            server_->stop();

            server_.reset();
            socket_.reset();
        }
    }

private:
    std::unique_ptr<Poco::Net::ServerSocket> socket_;
    std::unique_ptr<Poco::Net::HTTPServer> server_;
    std::string test_;
};

TEST_CASE("Get current weather") {
    FakeServer server("moscow weather");
    server.Start();

    auto obj = CreateYandexForecaster(kTestApiKey, server.GetUrl());
    auto forecast = obj->ForecastWeather(std::nullopt);

    REQUIRE(forecast.temp == -1);
    REQUIRE(forecast.feels_like == -5);
}

TEST_CASE("Lat-lon test") {
    FakeServer server("spb weather");
    server.Start();

    auto obj = CreateYandexForecaster(kTestApiKey, server.GetUrl());
    auto forecast = obj->ForecastWeather(Location{59.6, 30.2});

    REQUIRE(forecast.temp == 2);
    REQUIRE(forecast.feels_like == -6);
}

TEST_CASE("error") {
    FakeServer server("error");
    server.Start();

    auto obj = CreateYandexForecaster(kTestApiKey, server.GetUrl());
    REQUIRE_THROWS_AS(obj->ForecastWeather(std::nullopt), YandexAPIError);
}
